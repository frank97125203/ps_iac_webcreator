﻿[CmdletBinding()]
param
(
    [string] $mode,
    [string] $certPath,
    [string] $certDomain
)

# 載入WebAdministration
Import-Module WebAdministration

# 移除站台
function GetCertInfo
{
    param (
        [string] $certPath,
        [string] $certDomain
    )
    $cert = (Get-ChildItem "cert:\LocalMachine\$certPath" | where-object { $_.Subject -like "*$certDomain*" } | Select-Object -First 1).Thumbprint
    Write-Host $cert
}

try
{
    if(-Not ([string]::IsNullOrEmpty($mode)))
    {
        switch($mode.ToLower())
        {
            "getcert"
            {
                GetCertInfo $certPath $certDomain
            }
            default
            {
                Write-Host "沒有資料"
            }
        }
    }
}
catch [Exception]
{
    Write-Error ($_.Exception.Message)
}
finally
{
    Write-Host "完成!!!!!!!!!!!!!!!!!"
}