﻿[CmdletBinding()]
param
(
    [string] $jsonPath
)

# 載入WebAdministration
Import-Module WebAdministration
# 載入webSchema
$configPath = "$jsonPath"

# 建立AppPool
function CreateApplicationPool
{
    param (
        [string] $appPoolName,
        [string] $svcAccount,
        [string] $svcAccountPwd
    )
    Write-Host "正在建立AppPool $appPoolName"
    $appPoolPath = "IIS:\AppPools\$appPoolName"
    if(-Not (Test-Path -Path $appPoolPath))
    {
        # 設定RunTime 4.0、驗證模式、啟動模式、排程重啟
        New-WebAppPool $appPoolName | Set-ItemProperty -Name "managedRuntimeVersion" -Value "v4.0"
        Set-ItemProperty $appPoolPath -Name "processModel" -Value @{userName = $svcAccount; password = $svcAccountPwd; identitytype = 3}
        Set-ItemProperty $appPoolPath -Name "startMode" -Value "AlwaysRunning"
        Set-ItemProperty $appPoolPath -Name "Recycling.periodicRestart.schedule" -Value @{value="04:00"}
    }
    Write-Host "AppPool $appPoolName 建立完成"
}

# 建立站台
function CreateWebSite
{
    param
    (
        [string] $webSiteName,
        [string] $appPoolName,
        [string] $svcAccount,
        [string] $svcAccountPwd,
        [string] $physicalPath,
        [psobject] $bindingList
    )
    Write-Host "正在建立站台 $webSiteName"
    $sitePath = "IIS:\Sites\$webSiteName"
    if(-Not (Test-Path -Path $sitePath))
    {
        # 設定繫結
        $bindings = @()
        foreach($binding in $bindingList)
        {
            $bindings += @{protocol = $binding.Protocol; bindingInformation = "{0}:{1}:{2}" -f $binding.Ip, $binding.Port, $binding.HostHeader; SslFlags = $binding.SslFlags} 
        }
        New-Item $sitePath -Bindings $bindings -PhysicalPath $physicalPath -ApplicationPool $appPoolName
        Set-ItemProperty -Path $sitePath -Name userName -Value $svcAccount
        Set-ItemProperty -Path $sitePath -Name password -Value $svcAccountPwd
        
        # 綁定Ssl憑證
        foreach($binding in $bindingList)
        {
            if($binding.Protocol -Eq "https")
            {
                BindingSslCert $webSiteName $binding.HostHeader $binding.CertDomain $binding.CertPath 
            }
        }
    }
    Write-Host "站台 $webSiteName 建立完成"
}

# 建立虛擬目錄或應用程式
function CreateDirectory
{
    param
    (
        [string] $directoryName,
        [string] $appPoolName,
        [string] $svcAccount,
        [string] $svcAccountPwd,
        [string] $physicalPath,
        [string] $type
    )
    Write-Host "正在建立目錄 $directoryName"
    $directoryPath = "IIS:\Sites\$directoryName"
    if(-Not (Test-Path -Path $directoryPath))
    {
        switch($type.ToLower())
        {
            "application"
            {
                Write-Host "正在將目錄 $directoryName 轉為應用程式"
                CreateApplicationPool $appPoolName $svcAccount $svcAccountPwd
                New-Item $directoryPath -PhysicalPath $physicalPath -Type $type -ApplicationPool $appPoolName
                Write-Host "已完成轉換"
            }
            "virtualdirectory"
            {
                Write-Host "正在將目錄 $directoryName 轉為虛擬目錄"
                New-Item $directoryPath -PhysicalPath $physicalPath -Type $type
                Write-Host "已完成轉換"
            }
            default
            {
                Write-Host "type undefine"
            }
        }
        Set-ItemProperty -Path $directoryPath -Name userName -Value $svcAccount
        Set-ItemProperty -Path $directoryPath -Name password -Value $svcAccountPwd
    }
    Write-Host "目錄 $directoryName 建立完成"
}

# 憑證綁定
function BindingSslCert
{
    Param
    (
        [string] $webSiteName,
        [string] $hostHeader,
        [string] $certDomain,
        [string] $certPath
    )
    Write-Host "正在綁定 $hostHeader 憑證 $certDomain"
    $cert = (Get-ChildItem "cert:\LocalMachine\$certPath" | where-object { $_.Subject -like "*$certDomain*" } | Select-Object -First 1).Thumbprint
    $binding = Get-WebBinding -Name "$webSiteName" -Protocol "https" -HostHeader "$hostHeader";
    $binding.AddSslCertificate($cert, "$certPath")
    Write-Host "憑證 $certDomain 綁定完成"
}

try
{
    $configList = Get-Content $configPath | Out-String | ConvertFrom-Json
    # 建立站台
    foreach($web in $configList.WebSites)
    {
        CreateApplicationPool $web.AppPoolName $web.SvcAccount $web.SvcAccountPwd
        CreateWebSite $web.WebSiteName $web.AppPoolName $web.SvcAccount $web.SvcAccountPwd $web.PhysicalPath $web.Bindings
    }
    
    # 建立子站台或虛擬目錄
    foreach($directory in $configList.Directories)
    {
        CreateDirectory $directory.DirectoryName $directory.AppPoolName $directory.SvcAccount $directory.SvcAccountPwd $directory.PhysicalPath $directory.Type
    }
}
catch [Exception]
{
    Write-Error ($_.Exception.Message)
}
finally
{
    Write-Host "完成!!!!!!!!!!!!!!!!!"
}