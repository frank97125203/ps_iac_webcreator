[CmdletBinding()]
param($jsonPath)

# Get all inputs
$fullPath = "E:\"
$configPath = "$jsonPath"

function CreateFolder {
    param (
        [String] $folder
    )
    #判斷資料夾是否存在
    if (-Not (Test-Path -Path "$folder"))
    {
        Invoke-Expression -Command "mkdir $folder"
    }
}

function RemoveInheritance {
    param (
        [String] $folder
    )
    
    Write-Verbose "set access rule protection $folder"
    $acl = Get-ACL -Path $folder
    $acl.SetAccessRuleProtection($True, $True)
    (Get-Item $folder).SetAccessControl($acl)

    Write-Verbose "remove access rule $folder"
    $acl = Get-ACL -Path $folder
    $acl.Access `
    | Where-Object {$_.IdentityReference -ne "BUILTIN\Administrators" -and $_.IdentityReference -ne "$env:UserDomain\$env:UserName"} `
    | Foreach-Object {$acl.RemoveAccessRule($_) |Out-Null}
    
    (Get-Item $folder).SetAccessControl($acl)
}

function GrantAccessUsers {
    param (
        [string] $folder,
        [System.Object[]] $accessControlList
    )
    
    foreach ($accessUser in $accessControlList)
    {
        $identity = $accessUser.Identity
        Write-Verbose "grant access control $identity"

        $acl = Get-ACL -Path $folder
        $permission = $identity , $accessUser.FileSystemRights, $accessUser.InheritanceFlags, $accessUser.PropagationFlags, $accessUser.AccessControlType
        $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission
        $acl.SetAccessRule($accessRule)
        
        (Get-Item $folder).SetAccessControl($acl)
    }
}

try  
{
    Write-Verbose "user $env:UserDomain\$env:UserName"
    $configList = Get-Content $configPath | Out-String | ConvertFrom-Json
    Write-Host $configList.
    foreach($config in $configList.WebApp)
    {
        $folderName = $config.Name
        $iisName = $config.IIS

        CreateFolder "$fullPath\Web\$iisName\$folderName"

        Write-Host "##[section] remove inheritance $fullPath\Web\$iisName\$folderName"
        RemoveInheritance "$fullPath\Web\$iisName\$folderName"
        
        Write-Host "##[section] grant access users $fullPath\Web\$iisName\$folderName"
        GrantAccessUsers "$fullPath\Web\$iisName\$folderName" $config.AccessControlList
    }
    foreach($config in $configList.APData)
    {
        $folderName = $config.Name
        $iisName = $config.IIS

        CreateFolder "$fullPath\Static\$iisName\$folderName"

        Write-Host "##[section] remove inheritance $fullPath\Static\$iisName\$folderName"
        RemoveInheritance "$fullPath\Static\$iisName\$folderName"
        
        Write-Host "##[section] grant access users $fullPath\Static\$iisName\$folderName"
        GrantAccessUsers "$fullPath\Static\$iisName\$folderName" $config.AccessControlList
    }

    Write-Host "##[section] DONE"
}
catch [Exception] 
{
    Write-Error ($_.Exception.Message)
}