﻿[CmdletBinding()]
param
(
    [string] $mode,
    [string] $webPath
)

# 載入WebAdministration
Import-Module WebAdministration

# 移除站台
function RemoveWebSite
{
    param (
        [string] $webPath
    )
    Write-Host "正在移除站台 $webPath"
    Remove-Website -Name $webPath
    Write-Host "站台 $webPath 移除完成"
}
# 移除應用程式
function RemoveApplication
{
    param (
        [string] $webPath
    )
    Write-Host "正在移除應用程式 $webPath"
    Remove-WebApplication -Name $webPath
    Write-Host "應用程式 $webPath 移除完成"
}
# 移除虛擬目錄
function RemoveVirtualDirectory
{
    param (
        [string] $webPath
    )
    Write-Host "正在移除虛擬目錄 $webPath"
    Remove-WebVirtualDirectory -Name $webPath
    Write-Host "虛擬目錄 $webPath 移除完成"
}

try
{
    if(-Not ([string]::IsNullOrEmpty($mode)) -And -Not ([string]::IsNullOrEmpty($webPath)))
    {
        switch($mode.ToLower())
        {
            "website"
            {
                RemoveWebSite $webPath
            }
            "application"
            {
                RemoveApplication $webPath
            }
            "virtualdirectory"
            {
                RemoveVirtualDirectory $webPath
            }
            default
            {
                Write-Host "沒有移除資料"
            }
        }
    }
}
catch [Exception]
{
    Write-Error ($_.Exception.Message)
}
finally
{
    Write-Host "完成!!!!!!!!!!!!!!!!!"
}